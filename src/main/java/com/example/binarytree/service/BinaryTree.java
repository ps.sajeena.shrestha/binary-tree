package com.example.binarytree.service;

public class BinaryTree {

    private Node root;

    public BinaryTree() {
        root = null;
    }

    public boolean accept(int data) {
        if (findDepth(root, data) <= 0) {
            root = accept(root, data);
            return true;
        }
        return false;
    }

    private Node accept(Node root, int data) {
        if (root == null) {
            root = new Node(data);
        } else {
            if (data > root.getData()) {
                root.setRight(accept(root.getRight(), data));
            } else if (data < root.getData()) {
                root.setLeft(accept(root.getLeft(), data));
            }
        }
        return root;
    }

    public int depth(int data) {
        int depth = findDepth(root, data);
        if (depth < 0) {
            throw new IllegalStateException("");
        }
        return depth;
    }

    private int findDepth(Node root, int data) {
        int depth = -1;
        if (root == null) {
            return -1;
        }
        if (root.getData() == data
                || (depth = findDepth(root.getLeft(), data)) >= 0
                || (depth = findDepth(root.getRight(), data)) >= 0) {
            depth++;
        }
        return depth;
    }

    public int treeDepth() {
        if (root == null) {
            return 0;
        } else {
            return findTreeDepth(root);
        }
    }

    private int findTreeDepth(Node root) {
        int leftHeight = 0;
        int rightHeight = 0;
        if (root == null) {
            return -1;
        }
        if (root.getLeft() != null) {
            leftHeight = findTreeDepth(root.getLeft());
        }
        if (root.getRight() != null) {
            rightHeight = findTreeDepth(root.getRight());
        }
        return Math.max(leftHeight, rightHeight) + 1;
    }

}

package com.example.binarytree.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BinaryTreeTest {

    private BinaryTree binaryTree;

    @BeforeEach
    public void setUp() {
        binaryTree = new BinaryTree();
    }

    @Test
    @DisplayName("Given data in binary tree, when accept return true")
    public void givenDataInBinaryTree_whenAccept_thenReturnTrue() {
        assertTrue(binaryTree.accept(45), "Data should be accepted by the binary tree");
    }

    @Test
    @DisplayName("Given same data in binary tree, when accept return false")
    public void givenSameDataInBinaryTree_whenAccept_thenReturnFalse() {
        binaryTree.accept(50);
        binaryTree.accept(40);
        binaryTree.accept(15);
        assertFalse(binaryTree.accept(15), "Data should not be accepted by the binary tree");
    }

    @Test
    @DisplayName("Given data in binary tree, when depth return depth of the value")
    public void givenDataInBinaryTree_whenDepth_thenReturnDepthOfValue() {
        binaryTree.accept(4);
        binaryTree.accept(6);
        binaryTree.accept(8);
        binaryTree.accept(10);
        binaryTree.accept(18);
        binaryTree.accept(21);
        binaryTree.accept(15);

        int depth = binaryTree.depth(15);

        assertEquals(5, depth, "Depth of the value should be five");
    }

    @Test
    @DisplayName("Given root node only in binary tree, when depth return zero")
    public void givenRootNodeOnlyInBinaryTree_whenDepth_thenReturnZero() {
        binaryTree.accept(50);

        assertEquals(0, binaryTree.depth(50), "Depth of the value should be zero");
    }

    @Test
    @DisplayName("Given empty binary tree, when depth throw exception")
    public void givenEmptyBinaryTree_whenDepth_thenThrowException() {
        assertThrows(IllegalStateException.class, () -> binaryTree.depth(50),
                "It should throw exception");
    }


    @Test
    @DisplayName("Given three data in binary tree, when tree depth return depth of tree")
    public void givenThreeDataInBinaryTree_whenTreeDepth_thenReturnZero() {
        binaryTree.accept(50);
        binaryTree.accept(65);
        binaryTree.accept(30);

        int treeDepth = binaryTree.treeDepth();

        assertEquals(2, treeDepth, "Depth of the tree should be two");
    }

    @Test
    @DisplayName("Given empty binary tree, when tree depth return zero")
    public void givenEmptyBinaryTree_whenTreeDepth_thenReturnZero() {
        int treeDepth = binaryTree.treeDepth();

        assertEquals(0, treeDepth, "Depth of the tree should be zero");
    }

}